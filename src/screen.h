#pragma once

 // Abstract base class for low-level screen drawing
namespace Window
{
    class Screen
    {
        public:
    
            Screen() {};
            virtual ~Screen() {};       
    
            virtual void put_character(int y, int x, char symbol) = 0;
            virtual void update_screen() = 0;
    };
}
