#include "command.h"
#include "entity_config.h"
#include "cursesscreen.h"
#include "inputhandler.h"
#include "map.h"
#include "player.h"
#include "yaml_parser.h"
#include <iostream>
#include <memory>

int main()
{
    
    try
    {
        auto screen = Window::CursesScreen();
        auto map = Environment::Map(screen);

        auto config = Config::EntityConfig(map.get_current_map());
        map.draw(config);

        //auto player = Entity::Player(map.player_pos());
        auto player = Entity::Player();
        for(;;)
        {
             // Handle user inputs
            auto handler = Input::InputHandler(player,screen,map);

            Input::Command *button = handler.process_input();
            if (button) button->execute();
        }            
    }
    catch (std::runtime_error &e)
    {
        std::cout << e.what() << std::endl; 
        return 1;
    }
            
    return 0;
}
