#pragma once
#include "command.h"
#include "keys.h"
#include "player.h"
#include "map.h"

namespace Input
{

    class MoveCommand : public Command
    {

        public:
            MoveCommand() {};
            MoveCommand(Entity::Player *player,Window::Screen *s,Environment::Map *m);
            virtual ~MoveCommand();
            
            virtual void execute() = 0;

        protected:            
            bool process_interaction(int row,int col);
    };

    class MoveLeftCommand : public MoveCommand
    {
        public:
    
            MoveLeftCommand(Entity::Player *p,Window::Screen *s,Environment::Map *m);
            ~MoveLeftCommand();       
    
            void execute();
    
    };
    
    class MoveRightCommand : public MoveCommand
    {
        public:
    
            MoveRightCommand(Entity::Player *p,Window::Screen *s,Environment::Map *m);
            ~MoveRightCommand();       
    
            void execute();
    };
    
    class MoveUpCommand : public MoveCommand
    {
        public:
    
            MoveUpCommand(Entity::Player *p,Window::Screen *s,Environment::Map *m);
            ~MoveUpCommand();       
    
            void execute();
    };
    
    class MoveDownCommand : public MoveCommand
    {
        public:
    
            MoveDownCommand(Entity::Player *p,Window::Screen *s,Environment::Map *m);
            ~MoveDownCommand();       
    
            void execute();
    };
}
