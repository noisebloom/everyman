#pragma once
#include "keys.h"

 // NCurses class for low-level key input
class CursesKeys : public Keys
{
    public:

        CursesKeys();
        ~CursesKeys();       

        int get_input();
};

