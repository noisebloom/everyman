#include "inputhandler.h" 
#include "ncurses.h" 

 // CONSTRUCTOR
Input::InputHandler::InputHandler(Entity::Player &p,Window::Screen &s,Environment::Map &m) : player(&p),screen(&s),map(&m)
{
    set_default_keybindings();
}

 // DESTRUCTOR
Input::InputHandler::~InputHandler() 
{
    if (leftarrow != NULL) delete leftarrow;
    if (rightarrow != NULL) delete rightarrow;
    if (uparrow != NULL) delete uparrow;
    if (downarrow != NULL) delete downarrow;
}

void Input::InputHandler::set_default_keybindings()
{
     // TODO: Convert to smart pointers
    leftarrow = new MoveLeftCommand(player,screen,map);
    rightarrow = new MoveRightCommand(player,screen,map);
    downarrow = new MoveDownCommand(player,screen,map);
    uparrow = new MoveUpCommand(player,screen,map);
}

Input::Command *Input::InputHandler::process_input()
{
    auto keys = CursesKeys();
    int input = keys.get_input();

    switch (input)
    {
        case KEY_LEFT:
            return leftarrow;

        case KEY_RIGHT:
            return rightarrow;

        case KEY_DOWN:
            return downarrow;

        case KEY_UP:
            return uparrow;

        default: 
            return NULL;
    }                
}
