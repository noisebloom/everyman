#include "curseskeys.h"
#include <ncurses.h>

 // CONSTRUCTOR
CursesKeys::CursesKeys()
{
	keypad(stdscr, TRUE);
}

 // DESTRUCTOR
CursesKeys::~CursesKeys() {}

 // Get input from keyboard
int CursesKeys::get_input()
{
    return getch();
}
