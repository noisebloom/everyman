#include "movecommand.h" 
#include <iostream>

 // CONSTRUCTOR
Input::MoveCommand::MoveCommand(Entity::Player *p,Window::Screen *s,Environment::Map *m)
{
    player = p;
    screen = s;
    map = m;
}

 // DESTRUCTOR
Input::MoveCommand::~MoveCommand() {}

bool Input::MoveCommand::process_interaction(int row,int col)
{
    if (map->is_wall(row,col))
    {
        return true;
    }
    else if (map->is_entity(row,col))
    {
        // Interaction logic here
        map->interact_entity(row,col);
        return true;
    }

    return false;
}

 // CONSTRUCTOR
Input::MoveLeftCommand::MoveLeftCommand(Entity::Player *p,Window::Screen *s,Environment::Map *m) : MoveCommand(p,s,m) {}

 // DESTRUCTOR
Input::MoveLeftCommand::~MoveLeftCommand() {}

void Input::MoveLeftCommand::execute()
{
    std::tuple<int,int> pos = player->get_position();

     // Return true if there's something to "interact" with
    if (process_interaction(std::get<0>(pos),std::get<1>(pos)-1)) return;

    screen->put_character(std::get<0>(pos),std::get<1>(pos),' ');        
    screen->put_character(std::get<0>(pos),std::get<1>(pos)-1,player->get_player_char());
    screen->update_screen();

    player->update_position(std::make_tuple(std::get<0>(pos),std::get<1>(pos)-1));
}

 // CONSTRUCTOR
Input::MoveRightCommand::MoveRightCommand(Entity::Player *p,Window::Screen *s,Environment::Map *m) : MoveCommand(p,s,m) {}

 // DESTRUCTOR
Input::MoveRightCommand::~MoveRightCommand() {}

void Input::MoveRightCommand::execute()
{
    std::tuple<int,int> pos = player->get_position();

    if (process_interaction(std::get<0>(pos),std::get<1>(pos)+1)) return;

    screen->put_character(std::get<0>(pos),std::get<1>(pos),' ');        
    screen->put_character(std::get<0>(pos),std::get<1>(pos)+1,player->get_player_char());
    screen->update_screen();

    player->update_position(std::make_tuple(std::get<0>(pos),std::get<1>(pos)+1));
}

 // CONSTRUCTOR
Input::MoveUpCommand::MoveUpCommand(Entity::Player *p,Window::Screen *s,Environment::Map *m) : MoveCommand(p,s,m) {}

 // DESTRUCTOR
Input::MoveUpCommand::~MoveUpCommand() {}

void Input::MoveUpCommand::execute()
{
    std::tuple<int,int> pos = player->get_position();
    //std::cout << "row = " << std::get<0>(pos) << " col = " << std::get<1>(pos) << std::endl;

    if (process_interaction(std::get<0>(pos)-1,std::get<1>(pos))) return;

    screen->put_character(std::get<0>(pos),std::get<1>(pos),' ');        
    screen->put_character(std::get<0>(pos)-1,std::get<1>(pos),player->get_player_char());
    screen->update_screen();

    player->update_position(std::make_tuple(std::get<0>(pos)-1,std::get<1>(pos)));
}


 // CONSTRUCTOR
Input::MoveDownCommand::MoveDownCommand(Entity::Player *p,Window::Screen *s,Environment::Map *m) : MoveCommand(p,s,m) {}

 // DESTRUCTOR
Input::MoveDownCommand::~MoveDownCommand() {}

void Input::MoveDownCommand::execute()
{
    std::tuple<int,int> pos = player->get_position();

    if (process_interaction(std::get<0>(pos)+1,std::get<1>(pos))) return;

    screen->put_character(std::get<0>(pos),std::get<1>(pos),' '); 
    screen->put_character(std::get<0>(pos)+1,std::get<1>(pos),player->get_player_char());
    screen->update_screen();

    player->update_position(std::make_tuple(std::get<0>(pos)+1,std::get<1>(pos)));
}
