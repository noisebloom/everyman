#pragma once

 // Abstract base class for low-level key input
class Keys
{
    public:

        Keys() {};
        virtual ~Keys() {};       

        virtual int get_input() = 0;
};
