#pragma once
#include "movecommand.h"
#include "curseskeys.h"
#include "screen.h"
#include "map.h"
#include "player.h"

namespace Input
{
    class InputHandler
    {
        public:
    
            InputHandler(Entity::Player &p,Window::Screen &s,Environment::Map &m);
            ~InputHandler();       
    
            void set_default_keybindings();
            Command *process_input();
    
        private:
    
            Command *leftarrow;
            Command *rightarrow;
            Command *downarrow;
            Command *uparrow;
    
            Entity::Player *player;
            Window::Screen *screen;
            Environment::Map *map;
    };
}
