#pragma once
#include "player.h"
#include "screen.h"
#include "map.h"

namespace Input
{
    class Command
    {
        public:
    
            Command() {};
            Command(Entity::Player *player,Window::Screen *s);
            virtual ~Command() {};
    
            virtual void execute() = 0;
    
        protected:
    
            Entity::Player *player;
            Window::Screen *screen;
            Environment::Map *map;
    };
}
