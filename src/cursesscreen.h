#pragma once
#include "screen.h"
#include <ncurses.h>
#include <string>
#include <vector>

namespace Window
{
     // NCurses class for low-level key input
    class CursesScreen : public Screen
    {
        public:
    
            CursesScreen();
            ~CursesScreen();       
    
            void put_character(int y, int x, char symbol);
            void update_screen();
            void display_window(std::string str,std::vector<std::vector<char>> map);

        private:
            WINDOW *curr_win;
    };
}
