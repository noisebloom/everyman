#include "player.h"
#include <ncurses.h>
#include <iostream>

 // CONSTRUCTOR
Entity::Player::Player() : pos(10,10),player_char('e')
{
    start_color();
    init_pair(1, COLOR_GREEN, COLOR_BLACK);

	attron(COLOR_PAIR(1));

    mvaddch(std::get<0>(pos), std::get<1>(pos), player_char);
    refresh();
}

 // DESTRUCTOR
Entity::Player::~Player() {}

void Entity::Player::update_position(std::tuple<int,int> new_pos)
{
    pos = new_pos;
}
