#include "cursesscreen.h"

 // CONSTRUCTOR
Window::CursesScreen::CursesScreen()
{
	initscr();
	clear();
    curs_set(0);
    noecho();
    cbreak();
}

 // DESTRUCTOR
Window::CursesScreen::~CursesScreen()
{
     // Clear ncurses data structures
    endwin();
}

void Window::CursesScreen::put_character(int y, int x, char symbol)
{
    mvaddch(y,x,symbol);
}

void Window::CursesScreen::update_screen()
{
    refresh();
}

void Window::CursesScreen::display_window(std::string str,std::vector<std::vector<char>> map)
{
    int map_height = map.size();
    int map_width = map[0].size();

    int win_height = map_height / 2;
    int win_width = map_width / 2;

     // newwin(height,width,start_y,start_x)
    curr_win = newwin(win_height,win_width,map_height/5,map_width/5);

    box(curr_win,0,0);
    mvwprintw(curr_win,1,2,str.c_str());
    wrefresh(curr_win);
}
