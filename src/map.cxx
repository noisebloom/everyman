#include "map.h"
#include <filesystem>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <cctype>
#include <csignal>
#include <iostream>
#include <ncurses.h>

 // CONSTRUCTOR
Environment::Map::Map(Window::CursesScreen &s) : screen(&s),map_load_failures(0)
{
    if (! load_random_map("./data/world"))
        throw std::runtime_error("Your data directory is corrupt. Please reinstall Everyman.");
}

 // DESTRUCTOR
Environment::Map::~Map() {}

bool Environment::Map::load_random_map(std::string dir)
{
    if (map_load_failures > max_map_load_failures)
        return false;

    std::vector<std::string> maps;
    for(auto& p : std::__fs::filesystem::recursive_directory_iterator(dir))
    {
        if (p.path().string().find("map"))
            maps.push_back(p.path().string());
    }                

    std::stringstream ss;
    std::string map;

    curr_map_number = (rand() % maps.size())+1;

    ss << dir << "/map" << curr_map_number << "/map";
    ss >> map;
    if (! load_map(map))
    {
        ++map_load_failures;
        return load_random_map(dir);
    }
    return true;
}

bool Environment::Map::load_map(std::string path)
{
    std::ifstream infile(path);
    if (! infile.is_open())
        return false;

    int row = 0;
    char line[BUFSIZ];

    std::vector<char> tmp;
    std::vector<std::string> tmp2;
    while (infile.getline(line,BUFSIZ))
    {
         // TODO - This seems like a dumb way to initialize a vector, but I don't remember the "right" way
        map_data.push_back(tmp);
        map_data_type.push_back(tmp2);
        for (size_t col = 0; col < strlen(line); ++col)
        {
            map_data[row].push_back(line[col]);

            if (isdigit(line[col]))
                map_data_type[row].push_back("entity");
            else if (line[col] == ' ')
                map_data_type[row].push_back("empty");
            else
                map_data_type[row].push_back("wall");
        }                

        ++row;
    }

    infile.close();
    return true;
}


void Environment::Map::draw(Config::EntityConfig &cfg)
{
    config = &cfg;

    for (size_t row = 0; row < map_data.size(); ++row)
    {
        for (size_t col = 0; col < map_data[row].size(); ++col)
        {
            if (map_data_type.at(row).at(col) == "entity")
                screen->put_character(row,col,config->get_char(map_data[row][col]-ASCII_OFFSET));
            else
                screen->put_character(row,col,map_data[row][col]);
        }
    }
    refresh();
}

bool Environment::Map::is_wall(int row, int col)
{
    try
    {
        if (map_data_type.at(row).at(col) == "wall")
            return true;
    }                
    catch (const std::out_of_range &e)
    {
        return true;
    }

    return false;
}

bool Environment::Map::is_entity(int row, int col)
{
    try
    {
        if (map_data_type.at(row).at(col) == "entity")
            return true;
    }                
    catch (const std::out_of_range &e)
    {
        return true;
    }

    return false;
}

void Environment::Map::interact_entity(int row, int col)
{
    endwin();
    echo();

    screen->display_window(config->get_interaction(map_data[row][col]-ASCII_OFFSET),map_data);
    return;
}
