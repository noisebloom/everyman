#pragma once
#include <tuple>

namespace Entity
{
    class Player
    {
        public:
    
            Player();
            ~Player();       
    
            void move();            
            std::tuple<int,int> get_position() { return pos; }
            void update_position(std::tuple<int,int> new_pos);
            char get_player_char() { return player_char; }
    
        private:
    
            char player_char;
            std::tuple<int,int> pos;
    };
}
