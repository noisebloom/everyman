#pragma once
#include "cursesscreen.h"
#include "entity_config.h"
#include <tuple>
#include <string>
#include <vector>

namespace Environment
{
    class Map
    {
        public:
    
            Map(Window::CursesScreen &s);
            ~Map();       
    
            std::tuple<int,int> get_player_pos() { return player_pos; }
            void draw(Config::EntityConfig &cfg);
            bool is_wall(int row, int col);
            bool is_entity(int row, int col);

            void interact_entity(int row, int col);

            int get_current_map() { return curr_map_number; }
    
        private:
    
            bool load_random_map(std::string dir);
            bool load_map(std::string path);

            char get_map_object(int row, int col) { return map_data[row][col]; }
    
            std::tuple<int,int> player_pos;
            std::vector<std::vector<char>> map_data;
            std::vector<std::vector<std::string>> map_data_type;

            const size_t max_map_load_failures = 10;
            size_t map_load_failures;

            int curr_map_number;

            const int ASCII_OFFSET = 48;

            Window::CursesScreen *screen;
            Config::EntityConfig *config;
    };
}
