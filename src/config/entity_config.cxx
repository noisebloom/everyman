#include "entity_config.h"
#include "yaml_parser.h"
#include "yaml-cpp/yaml.h"
#include <filesystem>
#include <iostream>

 // CONSTRUCTOR
Config::EntityConfig::EntityConfig(int m) : map(m)
{
    parse_configs();
}

 // DESTRUCTOR
Config::EntityConfig::~EntityConfig() {}

void Config::EntityConfig::parse_configs()
{
    std::filesystem::path dir("./data/world/map" + std::to_string(map) + "/");

    if (! std::filesystem::is_directory(dir))
        throw std::runtime_error(dir.string() + " is not a directory");

    std::vector<std::string> file_list;
    
    for (const auto& file : std::filesystem::directory_iterator(dir))
    {
        const auto fullpath = file.path().string();
    
        if (! file.is_regular_file())
            continue;

        const auto filename = file.path().filename().string();

        if(filename.find(entity_basename) == 0)
        {
            int entity_num = stoi(filename.substr(entity_basename.size()));
            auto parser = YAMLParser(fullpath);

            entity_chars[entity_num] = stoi(parser.get_string("char"));

             // TODO - This needs to concatenate all the interaction options,
             // and we also need to store the pertinent fields. 
             //
             // A more robust structure is probably needed.
            entity_interact[entity_num] = parser.get_string("interact","msg");
        }
    }
}

char Config::EntityConfig::get_char(int entity_num)
{
    try
    {
        return entity_chars.at(entity_num);        
    }
    catch (std::out_of_range &e)
    {
        return ' ';
    }
}

std::string Config::EntityConfig::get_interaction(int entity_num)
{
    try
    {
        return entity_interact.at(entity_num);        
    }
    catch (std::out_of_range &e)
    {
        return "";
    }
}
