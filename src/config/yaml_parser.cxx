#include "yaml_parser.h"
#include <iostream>

 // CONSTRUCTOR
Config::YAMLParser::YAMLParser(std::string f) : file(f)
{
    config = YAML::LoadFile(file);   
}

 // DESTRUCTOR
Config::YAMLParser::~YAMLParser() {}

std::string Config::YAMLParser::check_type(std::string node)
{
    std::string type;

    switch (config[node].Type())
    {
        case YAML::NodeType::Null:
            type = "null";
            break;
        case YAML::NodeType::Scalar:
            type = "scalar";
            break;
        case YAML::NodeType::Sequence:
            type = "sequence";
            break;
        case YAML::NodeType::Map:
            type = "map";
            break;
        default:
            type = "undefined";
    }
    return type;
}

 // Node type is a scalar
std::string Config::YAMLParser::get_string(std::string node)
{
    return config[node].as<std::string>();
}

 // Node type is a map
std::string Config::YAMLParser::get_string(std::string node,std::string key)
{
    return config[node][key].as<std::string>();
}
std::vector<std::string> Config::YAMLParser::get_vector(std::string node,std::string key)
{
    return config[node][key].as<std::vector<std::string>>();
}
