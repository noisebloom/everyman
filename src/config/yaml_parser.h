#pragma once
#include <string>
#include "yaml-cpp/yaml.h"

namespace Config
{
    class YAMLParser
    {
        public:
    
            YAMLParser();
            YAMLParser(std::string f);
            ~YAMLParser();       

            std::string check_type(std::string key);
            std::string get_string(std::string key);
            std::string get_string(std::string node,std::string key);
            std::vector<std::string> get_vector(std::string node,std::string key);

        private:
            std::string file;

            YAML::Node config;
    };
}
