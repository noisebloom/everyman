#pragma once
#include <string>
#include <map>

namespace Config
{
    class EntityConfig
    {
        public:
            EntityConfig();
            EntityConfig(int m);
            ~EntityConfig();       

            char get_char(int entity_num);
            std::string get_interaction(int entity_num);

        private:
            void parse_configs();

            int map;
            const std::string entity_basename = "entity";
            std::map<int,int> entity_chars;
            std::map<int,std::string> entity_interact;
    };
}
