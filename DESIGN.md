# DESIGN 


- How do I handle complex decision trees? They also should be defined in YAML.

- Every NPC has base Rationality:
    - This can go up if you do things for them.
    - This will go down if you do things they don't like.
    - If it's low, they may fire you or refuse to do business. If it's very low, they may attack you. These are called Negative Reactions. There are also Positive Reactions.

- Jobs require a certain skill (string) and can grant skills

- People can give and take objects (string)

- Pizza Shop
    - Buy Pizza - straightforward, but if they are high rationality and you don't have enough, they may give you a free slice.
    - Steal Pizza - Utilizes your hidden stealth stat, if you are unsuccessful, their Rationality may cause them to forgive, refuse service next time, or attack
    - Apply For Job
        - No qualifications needed. They warn you to not "screw 'em over."
    - Deliver Pizza
        - If you have a job, you can deliver pizza to a pretermined location in X time.
        - If you don't come back with the money in X time, the entity will react based on Rationality.
        - The buyer may refuse to pay as well. Do you give them the pizza or not?

- Factions
    - You can join factions to get certain jobs or get automated boosts to skills:
        - Police: Boost to ranged weapons, strength
        - Thieves: Boost to stealth

    - You can join multiple factions, but factions may react negatively if they see you doing business with factions they dislike.

- Entity YAML design:
    # Entity parameters
    #
    # char: [integer for the ASCII character of the entity]
    # rationality: [how rational the entity is (0 - 10)]
    #
    # Interaction parameters
    # interact:
    #   msg: [message] This message is printed upon initial interaction
    #   buy: [message] [cost] This action automatically deducts cash from the player
    #   steal: [message] [quantity] [item] This action attempts to steal a quantity of an item
    #   [custom]: Any other field is custom and needs to be manually defined in the YAML below
     
    char: 64
    rationality: 8
    interact:
      msg: "Welcome to Lorenzo's Pizza!"
      buy: [ "Buy Pizza - $2", "2"]
      steal: [ "Steal Pizza", "8", "pizza"]
      deliver: "Deliver Pizza"
      apply: "Apply For a Job"
    
    # Custom interactions
    apply:
      msg: "Sure, the job's yours. Just don't screw me over, man."
      rolegrant: "pizzadelivery"
    
    deliver:
      rolerequire: "pizzadelivery"
