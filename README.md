# Everyman 
# v0.0

## Description

Everyman is an open-world ASCII game that empowers you to be the person you want to be.

## Building

    cmake -Bbuild -H. 
    cmake --build build
