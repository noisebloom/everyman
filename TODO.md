# TODO 

- Add Exception classes and have main.cxx drive the exception-handling and exiting.
- map: endwin() should not need to be called upon exit, since screen destructor calls it...
- DEBUG MODES! And local log file.
- YAML parsing for entities
- Enemies turn red when hostile
- Color files for each map to map each char to a color (maybe not NPCs)
